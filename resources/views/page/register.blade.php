@extends('layout.master')
@section('title')
Halaman Pendaftaraan Media Online
@endsection

@section('content')
	<form action="/sent" method="POST">
		@csrf
		<label>First name :</label>
		<input type="text" name="first_name"><br><br>
		<label>Last name :</label>
		<input type="text" name="last_name"><br><br>
		<label>Gender</label><br>
		<input type="radio">Male<br>
		<input type="radio">Female<br><br>
		<label>Nationality</label><br>
		<select name="Indonesia">
			<option value="Indonesia">Indonesia</option>
			<option value="amerika">Amerika</option>
			<option value="inggris">Inggris</option>
			<option value="Other">Other</option>
		</select><br><br>
		<label>Languange Spoken</label><br>
		<input type="checkbox" name="bahasa">Bahasa Indonesia<br>
		<input type="checkbox" name="english">English<br>
		<input type="checkbox" name="other">Other<br><br>
		<label>Bio</label><br>
		<textarea name="bio" rows="10" cols="30"></textarea><br>
		<input type="submit" name="submit" value="Sign Up">
	</form>
	<a href="/">Kembali Ke Halaman Utama</a>
@endsection