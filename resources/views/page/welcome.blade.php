@extends('layout.master')
@section('title')
Halaman Utama Media Online
@endsection

@section('content')
	<h1>SELAMAT DATANG, {{$first_name}} {{$last_name}}</h1>
	<h3>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h3>
@endsection