@extends('layout.master')
@section('title')
Halaman Tambah Cast
@endsection

@section('content')
<form method="POST" action="/cast">
  @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama">
  </div>
  @error('nama')
  <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="umur" class="form-control" placeholder="Masukkan Umur">
  </div>
  @error('umur')
  <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Masukkan Bio"></textarea>
  </div>
  @error('bio')
  <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection